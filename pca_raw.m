clear all
rng(1535, 'twister');

digitdata=[];
Y_gt = [];
for i = 0:9
    load(['digit' num2str(i)]);
    digitdata = [digitdata; single(D)];
    Y_gt = [Y_gt; ones(size(D, 1), 1) * (i + 1)];
end

disp('the trainset is going to be encoded with pca');
[eigvs, scores] = pca(digitdata);

scores = scores(:, 1 : 30);
eigvs = eigvs(:, 1 : 30);

restored = bsxfun(@plus, scores * eigvs', mean(digitdata));

mse = mean(sum((digitdata - restored) .^ 2, 2));

fprintf('MSE after pca: %d\n', mse);
