DO_PRETRAIN = true;
rng(1535, 'twister');

digitdata=[];
for i = 0:9
    load(['digit' num2str(i)]); 
    digitdata = [digitdata; single(D)];  %#ok<AGROW> only 10 times
end

nobjects = size(digitdata,1);
digitdata = digitdata(randperm(nobjects),:);  

% PUT YOUR CODE HERE
% describe logistic sigmoid and its derivative
arch1.actfun = @(X) bsxfun(@rdivide, 1, 1 + exp(-X));
arch1.dactfun = @(Y) Y .* (1 - Y);

clear arch;
arch(1:9) = arch1;
[arch.numw] = deal(784,1000,500,250,30,250,500,1000,784);

% override activation function for the middle layer
% PUT YOUR CODE HERE
% describe linnear function and its derivative
arch(5).actfun = @(X) X;
arch(5).dactfun = @(Y) ones(size(Y));

if DO_PRETRAIN
 tic;
 [w, b] = pretrain(arch, digitdata);
 toc
 wflat = flatten_weights(w, b);
else
 numw = [arch.numw]; %#ok<UNRCH>
 nweights = sum((numw(1:end-1)+1) .* numw(2:end));
 % PUT YOUR CODE HERE replace with random gaussian noise then try to
 % train without pretraining
 wflat = random('Normal', 0, 0.3, [nweights, 1]);
end

save('pretrain_weights.mat', 'wflat', 'arch');

disp('main neural network')

mse_hist = [];
time_hist = [];
starting_time = cputime;
save('train_err.mat', 'mse_hist', 'time_hist', 'starting_time');

tic;
f = ...
@(iter, Y) compute_error_and_plot(iter, Y, digitdata, 'train_err.mat');
wflat = ...
minimize(wflat, digitdata, digitdata, arch, false, 10, false, ...
         1500, f, 0.9);
toc

save('final_weights.mat', 'wflat', 'arch');
