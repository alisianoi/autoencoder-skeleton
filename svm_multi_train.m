function svms = svm_multi_train(X, Y, varargin)

    nobjects = size(X, 1);
    labels = unique(Y);
    nlabels = length(labels);
    svms = cell(1, nlabels - 1);

    for i = 1 : nlabels - 1
        svms{i} = cell(1, nlabels - i);
        iX = (Y == labels(i));
        for j = i + 1 : nlabels
            jX = (Y == labels(j));
            svms{i}{j - i} = ...
            svmtrain([X(iX, :); X(jX, :)], [Y(iX); Y(jX)], ...
                     varargin{:});
        end
    end
end
