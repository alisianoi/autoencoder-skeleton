clear all
rng(1535, 'twister');

disp('final_weights.mat is going to be loaded');
load('final_weights.mat');

digitdata=[];
Y_gt = [];
for i = 0:9
    load(['digit' num2str(i)]);
    digitdata = [digitdata; single(D)];
    Y_gt = [Y_gt; ones(size(D, 1), 1) * (i + 1)];
end

disp('the trainset is going to be encoded')
[~, scores] = pca(digitdata);
enc_digitdata = scores(:, 1 : 30);

%%%%%%%%% set architecture for 2-layer ann with softmax %%%%%%%%%
architecture(1).actfun = @(X) bsxfun(@rdivide, 1, 1 + exp(-X));
architecture(1).dactfun = @(Y) Y .* (1 - Y);
% a magic constant, 30 is the number of features after autoencoder
architecture(1).numw = 30;

architecture(2) = architecture(1);
% a magic number, there are 10 classes of objects
architecture(2).numw = 20;

architecture(3).actfun = @softmax;
architecture(3).dactfun = @dsoftmax;
% a magic number, there are 10 classes of objects
architecture(3).numw = 10;

numw = [30, 20, 10];
nweights = sum((numw(1 : end - 1) + 1) .* numw(2 : end));

% good old random recommendation
wflat2 = random('Normal', 0, 0.3, [nweights, 1]);

disp('2-layer neural network with softmax is about to learn');
grad_check = false;
nepochs = 30;
nbatches = 1500;
momentum = 0.97;
wflat2 = ...
ann_softmax_learn(wflat2, enc_digitdata, Y_gt, architecture, ...
                  grad_check, nepochs, nbatches, momentum);

disp('2-layer neural network with softmax has finished learning')

tied_w = false;
use_softmax = true;
Y = nnfunc(wflat2, enc_digitdata, Y_gt, architecture, tied_w, ...
           use_softmax);

[~, Y] = max(Y, [], 2);
fprintf('accuracy on train = %f\n', sum(Y == Y_gt) / size(Y, 1));

digitdata=[];
Y_gt = [];
for i = 0:9
  load(['test' num2str(i)]);
  digitdata = [digitdata; single(D)];
  Y_gt = [Y_gt; ones(size(D, 1), 1) * (i + 1)];
end

%% arch and wflat come from `final_weights.mat`
[~, scores] = pca(digitdata);
enc = scores(:, 1 : 30);

Y = nnfunc(wflat2, enc, Y_gt, architecture, false, true);

[~, Y] = max(Y, [], 2);
fprintf('accuracy on test = %f\n', sum(Y == Y_gt) / size(Y, 1));
