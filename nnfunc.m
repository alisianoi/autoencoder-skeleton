function [Y f df] = nnfunc(wflat, X, Y_gt, arch, tied_w, varargin)
% wflat: column vector of flattened weights ordered by layers.
%   Within each layer W{i} flattened by columns is followed by the
%   bias b{i}.  In case of tied weights (tied_w is true), the W{i}
%   parts are empty for the second half of layers (i.e. for the layers
%   from ceil(nlayers/2))
% X: feature matrix of size nobjects-by-nfeatures.
%   Its rows contain descriptions of objects.
% Y_gt: ground truth labels matrix of size nobjects-by-nlabels.
%   In case of the autoencoder, Y_gt == X.
% arch: a struct array that describes the architecture of the ANN of
%   size (nlayers+1).
%   arch(i).numw: number of weights on the i-th layer
%   arch(i).actfun: activation function on the i-th layer for i >= 2
%   arch(i).dactfun:gradient of the activation function on the i-th
%   layer.  It takes the VALUE of the activation function, not the
%   argument. i >= 2
% tied_w: flags if the weights are tied in the architecture
%   (default=false)
%
%  OUTPUT:
% Y: predicted labels matrix of size nobjects-by-nlabels.
% f: loss function (MSE) value of Y and Y_gt. 
% df: gradient of f as function of wflat. Column vector. Computed on
% demand.

  if nargin < 4
    error('Too few input aruments');
  else if nargin < 5
    tied_w = false;
  end

  %% proper optional argument parsing as in http://goo.gl/K10GN4
  numvarargs = length(varargin);
  if numvarargs > 1
     errId = 'nnfunc:TooManyArguments';
     errMsg = 'wflat, X, Y_gt, arch, tied_w, [softmax]';
     error(errId, errMsg);
  end

  gaps = cellfun(@isempty, varargin);
  optargs = {false};
  suboptargs = optargs(1 : numvarargs);

  varargin(gaps) = suboptargs(gaps);
  optargs(1 : numvarargs) = varargin;

  [use_softmax] = optargs{:};
  %% finished optional argument parsing

  nlayers = length(arch);
  nobjects = size(X, 1);

  % w and b are cell arrays 1-by-nlayers
  % each w{layer} perhaps looks like this: row contains all the weights
  % coming from a single neuron on the left to all the neurons on the
  % right. Each column is the weights coming from all the different
  % neurons on the left to a single neuron on the right.
  [w, b] = unflatten_weights(wflat, arch, tied_w);

  val = cell(1, nlayers);
  val{1} = X;

  for layer = 1 : nlayers - 1
    % PUT YOUR CODE HERE
    actfun = arch(layer + 1).actfun;

    val{layer + 1} = ...
    actfun(bsxfun(@plus, val{layer} * w{layer}, b{layer}));
  end

  Y = val{nlayers};

  if nargout < 2  % user is not interested in loss
      return
  end

  % PUT YOUR CODE HERE
  % compute loss value
  f = sum(sum((Y - Y_gt) .^ 2, 2)) / nobjects;

  if nargout < 3  % user is not interested in the gradient
      return
  end

  dactfun = arch(end).dactfun;
  if ~use_softmax
      locgrad = 2 * (Y - Y_gt) .* dactfun(Y);
  else
      nfeatures = size(Y, 2);

      % sice softmax is used on the last layer only, then just the
      % first computation of `local gradient` will change. Here's how:

      % get the Jacobian for every object, J is 10-by-10-by-nobjects
      J = dactfun(Y);

      % get the error. Since it's the last row, the error is as
      % before: a Y - Y_gt. The reshape prepares the error for matrix
      % multiplication
      R = reshape((Y - Y_gt)', 1, nfeatures, nobjects);

      % Now the local gradient. It used to be a diagonal matrix but
      % now it's a proper 10-by-10 matrix for each object.
      locgrad = ...
      reshape(sum(bsxfun(@times, J, R), 2), nfeatures, nobjects)';
  end

  for layer = nlayers - 1 : -1 : 1 % nlayers == length(arch)
      % a neat trick: compute the average sum of all objects at once
      dfdw{layer} = val{layer}' * locgrad / nobjects;
      % watch out for row-vector locgrad: we don't want to be summing
      % along the second dimension, only along the first one
      dfdb{layer} = sum(locgrad, 1) / nobjects;

      % remember: arch is longer than w (and b) by one object, hence
      % the `layer` index does not get an extra shift to the left
      dactfun = arch(layer).dactfun;
      locgrad = locgrad * w{layer}' .* dactfun(val{layer});
  end

  % if weights are tied, leave weights on the lower layers only,
  % empty the rest
  if tied_w
      % PUT YOUR CODE HERE
      % handle tied weights
      N = numel(dfdw);
      for i = 1 : N / 2
          dfdw{i} = dfdw{i} + dfdw{N + 1 - i}';
      end

      dfdw{N / 2 + 1 : end} = [];
  end

  df = flatten_weights(dfdw, dfdb);
  assert(length(df) == length(wflat));
  end
end
