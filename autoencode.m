function enc_dataset = autoencode(X, arch, wflat, varargin)

    numvarargs = length(varargin);
    if numvarargs > 6
       errId = 'autoencode:TooManyInputArguments';
       errMsg = 'X, [visual, nepochs, nbatches, momentum, savefile, callback_savefile]';
       error(errId, errMsg);
    end

    gaps = cellfun(@isempty, varargin);
    optargs = {false, 30, 1500, 0.97, ...
               'autoencode_sv.mat', ...
               'autoencode_callback_sv.mat'};
    suboptargs = optargs(1 : numvarargs);

    varargin(gaps) = suboptargs(gaps);
    optargs(1 : numvarargs) = varargin;

    [visual, nepochs, nbatches, momentum, savefile, callback_sv] = ...
    optargs{:};

    callback = ...
    @(iter, Y) compute_error_and_plot(iter, Y, X, callback_sv, visual);

    % chop the head off, leave layers from 1 up to the middle
    numw = [arch(1 : 5).numw];
    numweights = sum((numw(1 : end - 1) + 1) .* numw(2 : end));
    enc_dataset = ...
    nnfunc(wflat(1 : numweights), X, X, arch(1 : 5), false);

    save(savefile, 'enc_dataset');
end
