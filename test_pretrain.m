load digit0       % load only digit 0 instances
data = D;

% PUT YOUR CODE HERE
% describe logistic sigmoid and its derivative
arch1.actfun = @(X) bsxfun(@rdivide, 1, 1 + exp(-X));
arch1.dactfun = @(Y) Y .* (1 - Y);

clear arch
arch1.numw = 0;
arch(1:3) = arch1;
[arch.numw] = deal(784,1000,784);

numw = [arch.numw];
% weights are tied, so W matrices are stored only for the first half
% of layers
nweights = sum((numw(1 : end - 1) + 1) .* numw(2 : end)) - ...
  sum((numw(1 : end - 1)) .* numw(2 : end)) / 2;

% zero initialization. Try random gaussian instead
wflat = zeros(nweights, 1);
% break the symmetry
%% rng(1535, 'twister');
%% wflat = random('Normal', 0, 0.3, [nweights, 1]);

% this is for logging all error computations. See
% compute_error_and_plot function.
mse_hist = [];
time_hist = [];
starting_time = cputime;
save('pretrain_err.mat', 'mse_hist', 'time_hist', 'starting_time');

nepochs = 20;
nbatches = 1;
momentum = 0;
iter_callback = ...
@(iter, Y) compute_error_and_plot(iter, Y, data, 'pretrain_err.mat');

tic;
wflat = ...
minimize(wflat, data, data, arch, true, nepochs, false, nbatches, ...
         iter_callback, momentum);
toc
