digitdata=[];
for i = 0:9
    load(['test' num2str(i)]); 
    digitdata = [digitdata; D(1 : 500, :)];
end

nobjects = size(digitdata,1);
digitdata = digitdata(randperm(nobjects),:);  

% PUT YOUR CODE HERE
% describe logistic sigmoid and its derivative
arch1.actfun = @(X) bsxfun(@rdivide, 1, 1 + exp(-X));
arch1.dactfun = @(Y) Y .* (1 - Y);

clear arch;
arch(1:9) = arch1;
[arch.numw] = deal(784,1000,500,250,30,250,500,1000,784);

% override activation function for the middle layer
% PUT YOUR CODE HERE
% describe linnear function and its derivative
arch(5).actfun = @(X) X;
arch(5).dactfun = @(Y) ones(size(Y));

[~, f, ~] = nnfunc(wflat, digitdata, digitdata, arch, false)
