function [wflat fw] = ...
         minimize(wflat, X, Y_gt, arch, tied_w, nepochs, ...
                  grad_check, nbatches, iter_callback, momentum, ...
                  learning_rate, shuffle, varargin)
% wflat: column vector of flattened weights ordered by layers.  Within
%   each layer W{i} flattened by columns is followed by the bias b{i}.
%   In case of tied weights (tied_w is true), the W{i} parts are empty
%   for the second half of layers (i.e. for the layers from
%   ceil(nlayers/2))
% X: feature matrix of size nobjects x nfeatures.  Its rows contain
%   descriptions of objects.
% Y_gt: ground truth labels matrix of size nobjects x nlabels.  In
%   case of the autoencoder, Y_gt == X.
% arch: a struct array that describes the architecture of the ANN of
% size (nlayers+1).
% arch(i).numw: number of weights on the i-th layer arch(i).actfun:
%   activation function on the i-th layer for i >= 2 arch(i).dactfun:
%   gradient of the activation function on the i-th layer.  It takes
%   the VALUE of the activation function, not the argument. i >= 2
% tied_w: flags if the weights are tied in the architecture
%   (default=false)
% nepochs: number of training loops over the training set
%   (default=100) grad_check: flag showing if the gradient check is
%   necessary
% nbatches: number of batches training set is divided to on each
%   epoch.  Pass -1 if you want one instance per batch (default=-1)
% iter_callback: the function called several times per epoch to
%   monitor current training error. Takes the iteration number and
%   inferred labels
% momentum: training momentum (damping factor) (default=0.0)
% learning_rate: the factor stap on each iteration is multiplied by
%   (default=1.0)
% shuffle: flags if the training set is shuffled in the beginning of
%   each epoch. Useful for on-line and minibatch training (default=true)
%
%  OUTPUT:
% wflat: the updated parameters
% fw: loss function value on the returned wflat

  TEST_PERIOD = 250;
  nobjects = size(X, 1);
  
  if nargin < 4
    error('Too few input aruments');
  end
  if nargin < 5
    tied_w = false; 
  end
  if nargin < 6
    nepochs = 100;
  end
  if nargin < 7
    grad_check = true;
  end
  if nargin < 8
    nbatches = -1;
  end
  if nbatches == -1
    nbatches = nobjects;
  end
  if nargin < 9
    iter_callback = @(varargin) 0;  % by default, an empty callback
  end
  if nargin < 10
    momentum = 0.0; 
  end
  if nargin < 11
    learning_rate = 1;
  end
  if nargin < 12
    shuffle = true;
  end

  % the following lines are needed for manual stopping
  stopdlg = msgbox('Press OK to stop training after this batch');
  cleanupObj = onCleanup(@() cleandlg(stopdlg)); % destructor

  %% proper optional argument parsing as in http://goo.gl/K10GN4
  numvarargs = length(varargin);
  if numvarargs > 1
      errId = 'nnfunc:TooManyArguments';
      errMsg = 'wflat, X, Y_gt, arch, tied_w, [softmax]';
      error(errId, errMsg);
  end

  gaps = cellfun(@isempty, varargin);
  optargs = {false};
  suboptargs = optargs(1 : numvarargs);

  varargin(gaps) = suboptargs(gaps);
  optargs(1 : numvarargs) = varargin;

  [softmax] = optargs{:};
  %% finished optional argument parsing

  delta = zeros(length(wflat), 1);
  for epoch = 1 : nepochs
      fprintf('Epoch %d\n', epoch);

      perm = crossvalind('Kfold', nobjects, nbatches);

      for batch = 1 : nbatches
          idx = (perm == batch);
          cur_pass = nbatches * (epoch - 1) + batch;
          % periodically update. Please note that it's important to
          % compute the error over the whole dataset.
          if mod(batch, TEST_PERIOD) == 1
              Y = nnfunc(wflat, X, Y_gt, arch, tied_w, softmax);
              iter_callback(cur_pass, Y);
          end

          % PUT YOUR CODE HERE
          [~, fw, dfX] = ...
          nnfunc(wflat, X(idx, :), Y_gt(idx, :),arch, tied_w,softmax);

          if grad_check
              % PUT YOUR CODE HERE
              % choose a few weights (together with bias elements) and
              % numerically check the gradient
              dfXnumeric = ...
              numeric_grad(wflat, X(idx, :), Y_gt(idx, :), arch, ...
                           tied_w, fw, dfX);

              % N = ceil(nobjects / nbatches);
              % mse_grad = sum((dfX - dfXnumeric) .^ 2) / N;
              mse_grad = max(abs(dfX - dfXnumeric));
              fprintf('mse_grad: %d\n', mse_grad);
          end

          % PUT YOUR CODE HERE
          % update weights
          % wflat = wflat - step_size .* dfX;

          % compute decreasing step size; use learning_rate constant
          t = (epoch - 1) * nbatches + batch;
          step_size = learning_rate / t ^ (1 / 4);

          % momentum is a scalar, no worries.
          % delta = dfX;
          delta = (1 - momentum) * dfX + momentum * delta;
          wflat = wflat - step_size .* delta;

          drawnow
          if ~ishandle(stopdlg)
              break
          end
      end
  end
end

function cleandlg(stopdlg)
  if ishandle(stopdlg)
    close(stopdlg);
  end
end

function dfX = numeric_grad(wflat, X, Y_gt, arch, tied_w, fw, dfX, ...
                            varargin)
    delta = 1e-5;
    n2check = 200;

    wsz = size(wflat, 1);
    [~, choice] = crossvalind('LeaveMOut', wsz, n2check);

    for i = find(choice)'
        wdelta = [zeros(i - 1, 1); delta; zeros(wsz - i, 1)];
        [~, fwnumeric] = nnfunc(wflat + wdelta, X, Y_gt, arch, ...
                                tied_w, varargin{:});
        dfX(i) = (fwnumeric - fw) / delta;
    end
end
