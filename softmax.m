function Z = softmax(X)
    Z = bsxfun(@rdivide, exp(X), sum(exp(X), 2));
end
