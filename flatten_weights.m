function wflat = flatten_weights(w, b)
  if ~exist('b', 'var') || isempty(b)
    b = cell(1,length(w));
  end

  nlayers = length(w);
  assert(nlayers == length(b));
  
  wflat = [];
  for i = 1 : nlayers
      %#ok<AGROW> this is not called often
      wflat = [wflat; w{i}(:); b{i}(:)];
  end
end
