clear all
rng(1535, 'twister');
nobj_per_digit = 800;

%%%%%%%%% set up autoencoder architecture %%%%%%%%%
auto_arch.actfun = @(X) bsxfun(@rdivide, 1, 1 + exp(-X));
auto_arch.dactfun = @(Y) Y .* (1 - Y);
auto_arch.numw = 0;

auto_arch(1 : 9) = auto_arch;
[auto_arch.numw] = ...
deal(784, 1000, 500, 250, 30, 250, 500, 1000, 784);

auto_arch(5).actfun = @(X) X;
auto_arch(5).dactfun = @(Y) ones(size(Y));
%%%%%%%%% autoencoder architecture ready %%%%%%%%%
 
% %% the following control the fine-tuning of autoencoder. To control
% %% pretrain, go inside autoencode_learn.m

auto_visual = false;
auto_nepochs = 30;
auto_nbatches = 150;
auto_momentum = 0.97;

callback_save = 'pretrain_err.mat';

%% uncomment section below to recompute trainset encoding

%% this is for logging all error computations. See
%% compute_error_and_plot function.
mse_hist = [];
time_hist = [];
starting_time = cputime;
save(callback_save, 'mse_hist', 'time_hist', 'starting_time');

digitdata=[];
for i = 0:9
    load(['digit' num2str(i)]);
    digitdata = [digitdata; D(1 : nobj_per_digit, :)];
end

[enc_trainset, auto_wflat] = ...
autoencode_learn(digitdata, auto_arch, auto_visual, auto_nepochs, ...
                 auto_nbatches, auto_momentum, '', callback_save);

%% here is the stored result of autoencoding trainset
disp('loading trained autoencoder weights and encoded testset');
load('autoencode_learn_sv.mat');

Y_gt = reshape(repmat(1 : 10, nobj_per_digit, 1), [], 1);

disp('began learning svms for later one-vs-one classification');
svms = svm_multi_train(enc_trainset, Y_gt, 'kernel_function', ...
                       'linear', 'kktviolationlevel', 0.02);

res = sum(svm_multi_test(enc_trainset, svms) == Y_gt) / size(Y_gt, 1);
fprintf('one vs one strategy, train accuracy: %f\n', res);

digitdata=[];
for i = 0:9
  load(['test' num2str(i)]);
  digitdata = [digitdata; D(1 : nobj_per_digit, :)];
end

autoencode_test_sv = 'autoencode_test_sv.mat';

enc = autoencode(digitdata, auto_arch, auto_wflat, auto_visual, ...
                 auto_nepochs, auto_nbatches, auto_momentum, ...
                 autoencode_test_sv, callback_save);

load(autoencode_test_sv);

res = sum(svm_multi_test(enc, svms) == Y_gt) / size(Y_gt, 1);
fprintf('one vs one strategy, test accuracy: %f\n', res);
