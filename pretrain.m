function [w b] = pretrain(arch, data, varargin)

    numvarargs = length(varargin);
    if numvarargs > 5
       errId = 'pretrain:TooManyArguments';
       required = 'arch, data';
       optional = ', [visual, nepochs, nbatches, momentum, savefile, callback_savefile]';
       errMsg = [required optional];
       error(errId, errMsg)
    end

    gaps = cellfun(@isempty, varargin);
    optargs = {false, 3, 1500, 0.97, 'pretrain_err.mat', ...
               'pretrain_callback_sv.mat'};
    suboptargs = optargs(1 : numvarargs);

    varargin(gaps) = suboptargs(gaps);
    optargs(1 : numvarargs) = varargin;

    [visual, nepochs, nbatches, momentum, savefile, callback_sv] = ...
    optargs{:};

    % this is for logging all error computations. See
    % compute_error_and_plot function.
    mse_hist = [];
    time_hist = [];
    starting_time = cputime;
    save(savefile, 'mse_hist', 'time_hist', 'starting_time');

    nlayers = length(arch);
    for layer = 1 : nlayers / 2
        arch1 = [];
        arch2 = [];
        % PUT YOUR CODE HERE
        % compose arch1 of the three relevant layers
        arch1.actfun = arch(layer).actfun;
        arch1.dactfun = arch(layer).dactfun;
        arch1.numw = 0;

        arch2.actfun = arch(layer + 1).actfun;
        arch2.dactfun = arch(layer + 1).dactfun;
        arch2.numw = 0;

        % ouch, line below hurts!
        arch1(1:3) = [arch1, arch2, arch1];
        wborder = arch(layer).numw;
        wmiddle = arch(layer + 1).numw;
        [arch1.numw] = deal(wborder, wmiddle, wborder);

        numw = [arch1.numw];
        nweights = sum((numw(1:end-1)+1) .* numw(2:end)) - ...
                   sum((numw(1:end-1)) .* numw(2:end)) / 2;

        % PUT YOUR CODE HERE
        % initialize wflat with random gaussian noise
        wflat = random('Normal', 0, 0.3, [nweights, 1]);

        if visual && layer == 1
            iter_callback = ...
            @(iter,Y) compute_error_and_plot(iter, Y, data, ...
                                             savefile, true);
        else
            iter_callback = ...
            @(iter,Y) compute_error_and_plot(iter, Y, data, ...
                                             savefile,false);
        end

        wflat = ...
        minimize(wflat, data, data, arch1, true, nepochs, false, ...
                 nbatches, iter_callback, momentum);

        [w1, b1] = unflatten_weights(wflat, arch1, true);
        w{layer} = w1{1};
        w{nlayers - layer + 1} = w1{2};
        b{layer} = b1{1};
        b{nlayers - layer + 1} = b1{2};

        % PUT YOUR CODE HERE
        % compute input data for the next layer
        actfun = arch(layer + 1).actfun;

        data = ...
        actfun(bsxfun(@plus, data * w{layer}, b{layer}));
    end
end
