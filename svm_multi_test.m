function Y = svm_multi_test(X, svms)

    nlabels = size(svms, 2) + 1;
    nobjects = size(X, 1);
    votes = zeros(nobjects, nlabels);

    for i = 1 : nlabels - 1
        for j = i + 1 : nlabels
            % rely on the verdicts being usable matlab indexes
            verdicts = svmclassify(svms{i}{j - i}, X);

            idx = sub2ind(size(votes), [1 : nobjects]', verdicts);
            votes(idx) = votes(idx) + 1;
        end
    end

    [~, Y] = max(votes, [], 2);
end
