function wflat = ann_softmax_learn(wflat, Y, Y_gt, arch, varargin)
% neural network with softmax activation at last layer
% Y is a shrunk feature set, Y_gt are the corresponding digits

    numvarargs = length(varargin);
    if numvarargs > 7
       errId = 'ann_softmax_learn:TooManyInputArguments';
       required = 'Y, Y_gt';
       optional1 = ', [grad_check, nepochs, nbatches, momentum';
       optional2 = ', learning_rate, shuffle, savefile]';
       errMsg = required + optional1 + optional2;
       error(errId, errMsg);
    end

    gaps = cellfun(@isempty, varargin);
    optargs = {false, 30, 150, 0.9, 1, true, 'ann_softmax_sv.mat'};
    suboptargs = optargs(1 : numvarargs);

    varargin(gaps) = suboptargs(gaps);
    optargs(1 : numvarargs) = varargin;

    [grad_check, nepochs, nbatches, momentum, learning_rate, ...
     shuffle, savefile] = optargs{:};

    %%%% ugly code below to convert labels to `probabilities`
    i = size(Y, 1); j = 10;
    tmp = zeros(i, j)';
    Y_gt = Y_gt + [0 : j : (i - 1) * j]';
    tmp(Y_gt) = 1;
    Y_gt = tmp';
    %%%% ugly code finished

    callback = @(iter, Y) custom_callback(Y, Y_gt);

    wflat = minimize(wflat, Y, Y_gt, arch, false, nepochs, ...
                     grad_check, nbatches, callback, momentum, ...
                     learning_rate, shuffle, true);
end

function custom_callback(Y, Y_gt)
    nobjects = size(Y, 1);
    mse = sum(sum((Y - Y_gt) .^ 2, 2)) / nobjects;

    fprintf('MSE = %f\n', mse);
end
