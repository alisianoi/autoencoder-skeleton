function [enc_trainset, auto_wflat] = ...
         autoencode_learn(X, arch, varargin)

    numvarargs = length(varargin);
    if numvarargs > 6
       errId = 'autoencode_learn:TooManyInputArguments';
       required = 'X, arch';
       errMsg = ', [visual,nepochs,nbatches,momentum,savefile,callback_savefile]';
       error(errId, errMsg);
    end

    gaps = cellfun(@isempty, varargin);
    optargs = {false, 30, 150, 0.97, ...
               'autoencode_learn_sv.mat', ...
               'autoencode_learn_callback_sv.mat'};
    suboptargs = optargs(1 : numvarargs);

    varargin(gaps) = suboptargs(gaps);
    optargs(1 : numvarargs) = varargin;

    [visual, nepochs, nbatches, momentum, savefile, callback_sv] = ...
    optargs{:};

    pretrain_nepochs = 5;
    pretrain_nbatches = 150;
    pretrain_momentum = 0.97;
    [w, b] = ...
    pretrain(arch, X, visual, pretrain_nepochs, pretrain_nbatches, ...
             pretrain_momentum, savefile);
    wflat = flatten_weights(w, b);

    callback = ...
    @(iter, Y) compute_error_and_plot(iter, Y, X, callback_sv, visual);

    auto_wflat = ...
    minimize(wflat, X, X, arch, false, nepochs, false, nbatches, ...
             callback, momentum);

    % chop the head off, leave layers from 1 up to the middle
    numw = [arch(1 : 5).numw];
    numweights = sum((numw(1 : end - 1) + 1) .* numw(2 : end));
    enc_trainset = ...
    nnfunc(wflat(1 : numweights), X, X, arch(1 : 5), false);

    save(savefile, 'enc_trainset', 'auto_wflat');
end
