function J = dsoftmax(Y)
% returns a nfeatures-by-nfeatures-by-nobjects tensor
% based on this crossvalidated question http://goo.gl/lHTAml

    [nobjects, nfeatures] = size(Y);
    R = reshape(Y', 1, nfeatures, nobjects);
    I = repmat(eye(nfeatures, nfeatures), [1, 1, nobjects]);
    J = ...
    bsxfun(@times, bsxfun(@minus, I, R), reshape(R, nfeatures, 1, []));
end
